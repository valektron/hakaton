<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// Portal
//Route::prefix('/api')->middleware(['web'])->namespace('Api')
//    ->group(function () {
//        Route::get('/call', 'CallController@index');
//        Route::get('/{userId}/calls', 'CallController@calls');
//        Route::get('/call/create', 'CallController@create');
//    });
