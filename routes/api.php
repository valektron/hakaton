<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')
    ->group(function () {
        Route::get('/call', 'CallController@index')->name('index');
        Route::get('/{userId}/calls', 'CallController@calls');
        Route::get('/call/history', 'CallController@history');
        Route::get('/call/end', 'CallController@end');
        Route::post('/call/update', 'CallController@update');
        Route::get('/call/statistics', 'CallController@statistics');
        Route::get('/call/finished', 'CallController@finished');
    });


Route::get('/new-call', 'Api\CallController@notify');
