<?php

namespace App;

use App\Model\Call;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Return last calls for this user
     * @param int $count
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lastCalls($count = 10)
    {
        return $this->hasMany(Call::class)->orderByDesc('id')->limit(intval($count));
    }
}
