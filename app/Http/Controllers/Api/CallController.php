<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\User;
use App\Model\Call;
use App\Events\EndCall;
use App\Events\NewCall;
use App\Http\Controllers\Controller;

use Symfony\Component\Process\Process;

class CallController extends Controller
{

    const URL_GET_NAME_BY_PHONE = 'http://venni.chocodev.kz/sandbox/get_user_name_by_phone';
    const STATUS_NEW = 'new';
    const STATUS_FINISHED = 'finished';
    const STATUS_PROCESSED = 'processed';
    const STATUS_ADEQUATE = 'adequate';
    const STATUS_INADEQUATE = 'inadequate';

    /**
     * Create new call data
     * Сработает когда Asterix поймает входящий звонок
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->all('phone');

        $data['name'] = file_get_contents(self::URL_GET_NAME_BY_PHONE.'?phone='.$data['phone']);
        if (empty($name)) {
            $data['name'];
        }

        $data['opened_at'] = now();
        $data['status'] = self::STATUS_NEW;

        // Send data for frontend
        event(new NewCall($data));

        $newCall = Call::create($data);

        if (!$newCall) {
            return response('No created', 400);
        }

        return response($newCall->id);
    }

    /**
     * End call
     * Сработает когда Asterix поймет, что звонок завершен
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function end(Request $request)
    {
        $data = $request->all('phone');

        $lastCall = Call::findByPhone($data['phone'], 1)->last();

        $lastCall->closed_at = now();
        $lastCall->status = self::STATUS_FINISHED;
        $lastCall->save();

        $data['id'] = $lastCall->id;
        $data['name'] = $lastCall->name;
        $data['duration'] = $lastCall->duration;

        // Send data for frontend
        event(new EndCall($data));

        return response($lastCall->id);
    }

    /**
     * Update - call
     * Сработает когда Asterix обработает звуковую дорожку и распарсит ее
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all('phone', 'talk', 'file_id');

        $lastCall = Call::findByPhone($data['phone'], 1)->last();

        $lastCall->talk = $data['talk'] ?? '';
        $lastCall->file_id = $data['file_id'] ?? '';
        $lastCall->analyze_words = $this->analyzeText($data['talk']);

        if ( ! empty($lastCall->analyze_words)) {
            $lastCall->status = self::STATUS_INADEQUATE;
        } else {
            $lastCall->status = self::STATUS_ADEQUATE;
        }

        $lastCall->save();

        return response($lastCall->id);
    }


    /**
     * Response 10 last calls by user
     * Вернет последние обращения по нужному пользователю
     *
     * @param User    $user
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @deprecated - вроде не юзаем, пока не удаляю
     */
    public function calls(User $user, Request $request)
    {
        $calls = $user->lastCalls();

        return response()->json($calls);
    }

    /**
     * Get calls history
     * Вернет последние звонки совершенные с данного номера
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function history(Request $request)
    {
        $phone = $request->get('phone');
        $calls = Call::findByPhone($phone);

        return response()->json($calls);
    }

    /**
     * Обработка строки которую возвращает mystem после анализа
     *
     * @param $s
     * @return bool|string
     */
    private function removeQuestionMark($s)
    {
        if (substr($s, strlen($s) - 1, strlen($s)) == '?') {
            $s = substr($s, 0, strlen($s) - 1);
        }
        return $s;
    }

    /**
     * Определяет, относится ли слово к матерным словам
     * @param $s
     * @return bool
     */
    private function isBannedWord($s)
    {
        $bannedWords = file(base_path('public/obscene_corpus.txt'), FILE_IGNORE_NEW_LINES);

        if (is_array($s)) {
            foreach ($s as $word) {
                if (in_array(mb_strtoupper($word), $bannedWords)) {
                    return TRUE;
                }
            }
        } else {
            if (in_array(mb_strtoupper($s), $bannedWords)) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Обрабатывает исходный текст и возвращает проанализированные слова в тексте
     * @param $text
     * @return array
     */
    protected function analyzeText($text)
    {
        $process = new Process('echo "' . $text . '" | mystem -l /dev/stdin');
        $process->run();

        $original = preg_split('/\s+/', $text, -1, PREG_SPLIT_NO_EMPTY);

        $str = explode('}', $process->getOutput());

        $r = [];

        foreach ($str as $key => $value) {
            $str[$key] = substr($value, 1, strlen($value));

            if (strpos($str[$key], '|') !== FALSE) {
                $words = explode('|', $str[$key]);
                foreach ($words as &$word) {
                    $word = $this->removeQuestionMark($word);
                }
                if ($this->isBannedWord($words)) {
                    $r[] = $original[$key];
                }
            } else {
                $str[$key] = $this->removeQuestionMark($str[$key]);

                if ($this->isBannedWord($str[$key])) {

                    $r[] = $original[$key];
                }
            }
        }

        return $r;
    }

    /**
     * Вернет данные для раздела со статистикой
     *
     * @return Call[]|\Illuminate\Database\Eloquent\Collection
     */
    public function statistics()
    {
        $lastCalls = Call::all()->sortByDesc('id');
        return response()->json($lastCalls);
    }

    /**
     * Сохранение формы после того как завершится звонок и менеджер дополнит данные
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function finished(Request $request)
    {
        $data = $request->all('theme_dialog', 'is_warning', 'id');

        $call = Call::find(intval($data['id']));
        $call->is_warning = !empty($data['is_warning']);
        $call->theme_dialog = $data['theme_dialog'] ?? '';
        $call->save();

        return redirect('http://localhost:4200');
    }
}
