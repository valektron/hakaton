<?php

namespace App\Model;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Concern\CallConcern;

/**
 * @property Carbon closed_at
 * @property Carbon opened_at
 */
class Call extends Model
{
    use CallConcern;

    protected $fillable = [
        'phone',
        'name',
        'created_at',
        'updated_at',
        'opened_at',
        'closed_at',
        'talk',
        'file_id',
        'status',
        'analyze_words',
        'is_warning',
        'theme_dialog',
    ];

    protected $visible = [
        'id',
        'phone',
        'name',
        'created_at',
        'updated_at',
        'opened_at',
        'closed_at',
        'talk',
        'file_id',
        'status',
        'analyze_words',
        'duration',
        'is_warning',
        'theme_dialog',
        'is_analyzed_front',
    ];

    protected $appends = ['duration', 'is_analyzed_front'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'opened_at',
        'closed_at',
    ];

    /**
     * Кастомное поле для duration
     * @return string
     */
    public function getDurationAttribute()
    {
        $duration = null;
        if( $this->opened_at && $this->closed_at) {
            $duration = $this->closed_at->timestamp - $this->opened_at->timestamp;
            $duration .= ' сек.';
        }
        return $duration;
    }

    /**
     * Кастомное поле для is_analyzed_front (Чисто для фронтенда, временно)
     * @return string
     */
    public function getIsAnalyzedFrontAttribute()
    {
        return false;
    }

    /**
     * Мутатор для поля analyze_words
     * @param $value
     * @return mixed
     */
    public function getAnalyzeWordsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Мутатор для поля analyze_words
     * @param $value
     * @return mixed
     */
    public function setAnalyzeWordsAttribute($value)
    {
        $this->attributes['analyze_words'] = json_encode($value);
    }

    /**
     * Return the manager user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function manager()
    {
        return $this->hasOne(User::class, 'id', 'manager_id');
    }
}
