<?php

namespace App\Concern;

use App\Model\Call;

trait CallConcern
{
    /**
     * Find history calls by phone number
     * @param string $phone
     * @param int $limit
     * @return mixed
     */
    public static function findByPhone($phone, $limit = 10)
    {
        return self::wherePhone($phone)->limit($limit)->orderByDesc('id')->get();
    }
}
